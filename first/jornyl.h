#ifndef JORNYL_H
#define JORNYL_H

#include <QDialog>

namespace Ui {
class Jornyl;
}

class Jornyl : public QDialog
{
    Q_OBJECT

public:
    explicit Jornyl(QWidget *parent = nullptr);
    ~Jornyl();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::Jornyl *ui;
};

#endif // JORNYL_H
