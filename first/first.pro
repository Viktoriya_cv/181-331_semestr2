#-------------------------------------------------
#
# Project created by QtCreator 2019-04-20T13:23:57
#
#-------------------------------------------------

QT       += core gui
QT += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = first
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    trener.cpp \
    lesson.cpp \
    trener_a.cpp \
    vrem.cpp \
    add.cpp \
    trener.cpp \
    trenernew.cpp \
    admin.cpp \
    client.cpp \
    jornyl.cpp \
    registration.cpp

HEADERS += \
        mainwindow.h \
    trener.h \
    lesson.h \
    trener_a.h \
    vrem.h \
    add.h \
    admin.h \
    client.h \
    jornyl.h \
    registration.h

FORMS += \
        mainwindow.ui \
    trener.ui \
    lesson.ui \
    trener_a.ui \
    vrem.ui \
    add.ui \
    admin.ui \
    client.ui \
    jornyl.ui \
    registration.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
