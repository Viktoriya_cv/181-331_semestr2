#include "trener_a.h"
#include "ui_trener_a.h"
#include "QFile"
#include "admin.h"
//#include "addnew.h"
#include <QTextStream>
#include <QMessageBox>
#include "add.h"

using namespace std;

trener_a::trener_a(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::trener_a)
{
    ui->setupUi(this);
    QFile file("C:\\Users\\Vika\\Desktop\\first\\first\\Trener.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }
}

trener_a::~trener_a()
{
    delete ui;
}

void trener_a::on_pushButton_clicked() // добавление тренера
{
    hide();
    add addtrener;
    addtrener.setModal(true);
    addtrener.exec();

}

void trener_a::on_pushButton_2_clicked()  // выход в меню администратора
{

    hide();
    admin ad;
    ad.setModal(true);
    ad.exec();

}
