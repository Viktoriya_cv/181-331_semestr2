#include "add.h"
#include "ui_add.h"
#include <fstream>
#include <QMessageBox>
#include "trenernew.cpp"
#include "trener_a.h"

using namespace std;

add::add(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add)
{
    ui->setupUi(this);
}

add::~add()
{
    delete ui;
}

void add::on_pushButton_clicked()
{
    //setlocale(LC_ALL, "Russian");
    QString fio  = ui->lineEdit ->text();
    QString sport = ui->lineEdit_2 ->text();
    QString abon = ui->lineEdit_3 -> text();
   //string fio_o = fio.toUtf8().constData();
   //string sport_o=sport.toUtf8().constData();

   TRENER new_element = TRENER(fio, sport, abon);

   new_element.add_in_db();

   QMessageBox::information(this, "Тренер добавлен", "Выполнено!");
   ui->lineEdit->setText("");
   ui->lineEdit_2->setText("");
   ui->lineEdit_3->setText("");

   hide();
   trener_a tr_a;
   tr_a.setModal(true);
   tr_a.exec();

}
