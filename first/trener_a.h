#ifndef TRENER_A_H
#define TRENER_A_H

#include <QDialog>

namespace Ui {
class trener_a;
}

class trener_a : public QDialog
{
    Q_OBJECT

public:
    explicit trener_a(QWidget *parent = nullptr);
    ~trener_a();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::trener_a *ui;
};

#endif // TRENER_A_H
