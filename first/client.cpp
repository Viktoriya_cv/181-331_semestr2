#include "client.h"
#include "ui_client.h"
#include "trener.h"
#include "lesson.h"

client::client(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::client)
{
    ui->setupUi(this);
}

client::~client()
{
    delete ui;
}

void client::on_pushButton_clicked()
{
    hide();
    Trener trener;
    trener.setModal(true);
    trener.exec();

}

void client::on_pushButton_2_clicked()
{
    hide();
    lesson les;
    les.setModal(true);
    les.exec();

}

void client::on_pushButton_3_clicked()
{
    hide();
}
