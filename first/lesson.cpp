#include "lesson.h"
#include "ui_lesson.h"
#include "client.h"
#include "QFile"


using namespace std;

lesson::lesson(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::lesson)
{
    ui->setupUi(this);
    QFile file("C:\\Users\\Vika\\Desktop\\first\\first\\Data.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }
}

lesson::~lesson()
{
    delete ui;
}

void lesson::on_pushButton_clicked()
{
    hide();//закрывает первое окно
    client Client1;
    Client1.setModal(true);
    Client1.exec();
}
