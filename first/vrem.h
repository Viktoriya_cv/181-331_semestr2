#ifndef VREM_H
#define VREM_H

#include <QDialog>

namespace Ui {
class vrem;
}

class vrem : public QDialog
{
    Q_OBJECT

public:
    explicit vrem(QWidget *parent = nullptr);
    ~vrem();

private slots:
    void on_pushButton_clicked();

private:
    Ui::vrem *ui;
};

#endif // VREM_H
