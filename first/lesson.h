#ifndef LESSON_H
#define LESSON_H

#include <QDialog>

namespace Ui {
class lesson;
}

class lesson : public QDialog
{
    Q_OBJECT

public:
    explicit lesson(QWidget *parent = nullptr);
    ~lesson();

private slots:
    void on_pushButton_clicked();

private:
    Ui::lesson *ui;
};

#endif // LESSON_H
