#include "admin.h"
#include "ui_admin.h"
#include "trener_a.h"
#include "vrem.h"
#include <jornyl.h>


using namespace std;

admin::admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::admin)
{
    ui->setupUi(this);
}

admin::~admin()
{
    delete ui;
}

void admin::on_pushButton_clicked()
{
    hide();
    trener_a tr_a;
    tr_a.setModal(true);
    tr_a.exec();
}

void admin::on_pushButton_2_clicked()
{

        hide();
        vrem dat;
        dat.setModal(true);
        dat.exec();

}

void admin::on_pushButton_3_clicked()
{

    hide();

}

void admin::on_pushButton_4_clicked()
{
    hide();
    Jornyl jor;
    jor.setModal(true);
    jor.exec();

}

void admin::on_pushButton_5_clicked()
{
    /*hide();
    baza base;
    base.setModal(true);
    base.exec();*/
}
