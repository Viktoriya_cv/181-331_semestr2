#include "vrem.h"
#include "ui_vrem.h"
#include "admin.h"
#include "QFile"

using namespace std;

vrem::vrem(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::vrem)
{
    ui->setupUi(this);
    QFile file("C:\\Users\\Vika\\Desktop\\first\\first\\Data.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }
}

vrem::~vrem()
{
    delete ui;
}

void vrem::on_pushButton_clicked()
{
    hide();
    admin ad;
    ad.setModal(true);
    ad.exec();

}
