#ifndef TRENER_H
#define TRENER_H

#include <QDialog>

namespace Ui {
class Trener;
}

class Trener : public QDialog
{
    Q_OBJECT

public:
    explicit Trener(QWidget *parent = nullptr);
    ~Trener();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::Trener *ui;
};

#endif // TRENER_H
